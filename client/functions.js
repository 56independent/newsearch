function sendData(requestData) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain',
            'Content-Length': requestData.length.toString()
        },
        body: requestData
    };

    return fetch('http://127.0.0.1:5000/', requestOptions)
    .then(response => response.text())
    .then(data => {
        console.log('Response:', data);
        return data;
    })
    .catch(error => {
        console.error('Error:', error);
        throw error;
    });
}

function isSubset(subset, superset) {
    return subset.every(item => superset.includes(item));
}

function makeCardsFrom2dWebsiteArray(websites, element){
    for (let i = 0; i < websites.length; i++) {
        const website = websites[i];
        console.log(website);
        element.append(`
                <div class="card m-3" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title"><a href="http://${website[0]}">${website[0]}</a></h5>
                        <p class="card-text">${website[1]}</p>
                        <a href="websites.html?website=${website[0]}" class="btn btn-primary">Read more!</a>
                        <p><code>${JSON.stringify(website[3])}</code></p>
                    </div>
                </div>
            `)

        $(".card-body").children().addClass("m-2")
    }
}

function unBoringise(input) {
    // Replace dashes with spaces
    var stringWithSpaces = input.replace(/-/g, ' ');

    // Make it sentence case
    var sentenceCase = stringWithSpaces.toLowerCase().replace(/^(.)|\s+(.)/g, function($1) {
        return $1.toUpperCase();
    });

    // Replace underscores with dashes
    var finalString = sentenceCase.replace(/_/g, '-');

    return finalString;
}

async function findNewTags(taglist) {
    try {
      const request = {
        "type": "tag-list",
        "key": "ZHt_VYRcoViZ75WFWhT9zA",
        "data": {}
      };
  
      const data = await sendData(JSON.stringify(request));
      const processed = JSON.parse(data);
      const serverTags = Object.keys(processed.data.tags);
  
      return taglist.filter(element => !serverTags.includes(element));
    } catch (error) {n
      $("#remove").text("An error occurred!<br>" + error);
      console.error('Error:', error);
      return ["tag-list unavailable;", error];
    }
}
  

function writeList(list, unordered){
    unordered = unordered || true;

    console.log(list)

    if (unordered){
        listBody = $("<ul>")
    } else {
        listBody = $("<ol>")
    }

    for (i = 0; i < list.length; i++){
        listBody.append(
            $("<li>")
                .text(list[i])
        )
    }

    return listBody
}