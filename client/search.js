$("body").append(`
<div class="container m-5" id="forResults">
<div class="row justify-content-center">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
<div class="input-group">
<input type="text" id="searchBox" class="form-control" placeholder="open-source game">
<button id="search" class="btn btn-outline-secondary">Submit</button>
</div>
</div>
</div>
</div>
`);

$("#search").click(function () {
    search = $("#searchBox").val()
    
    const searchFor = search.split(" ");

    errantTags = findNewTags(searchFor)

    console.log(errantTags)

    if (errantTags.length == 0){
        text = $("<p>Search valid!</p>").css({"color":"green"})
        error = false;
    } else {
        text = $("<p>Search invalid! You should not have these tags:</p>")
        text.css({"color":"red"})

        list = writeList(errantTags)

        error = true;
    }

    $("#forResults")
        .append(
            $("<div>")
            .attr("class", "row justify-content-center text-center")
            .attr("id", "verification")
            .append(
                text.fadeIn(200)
            )
            .append(list)
        )

    if (!error){
        newRequest = {
            "type":"search",
            "key":"iFIuo0B7urBzRUpJSNy7RQ",
            "data":{
                "tags":searchFor
            }
        }

        sendData(JSON.stringify(newRequest)).then(data => {
            processed = JSON.parse(data)
            websites = processed.data

            place = $("<div>")
                .attr("class", "row justify-content-center text-center")
                .attr("id", "verification")

            makeCardsFrom2dWebsiteArray(websites, place)

            $("#forResults").append(place)

            $("#verification").fadeOut()
        })
    }
})