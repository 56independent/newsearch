$("body").append(`
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.html">NewSearch</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.html">Index</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="tags.html">Tag index</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="search-results.html">Search</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="browse.html">Browse pages</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="create.html">Creaté pages</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
`)