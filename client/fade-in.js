animate = () => {
  // Set initial offset and delay values
  var offset = 100; // Offset in pixels
  var delay = 200; // Delay in milliseconds

  // Select the elements to slide in
  var elements = $('.animate');

  // Set initial CSS properties for the elements
  elements.css({
    "opacity": 0
  });

  // Loop through each element and apply the slide-in effect
  elements.each(function(index) {
    var element = $(this);

    // Calculate the delay for each element
    var currentDelay = index * delay;

    // Apply animation with delay
    setTimeout(function() {
      element.animate({
        "opacity": 1
      }, 500); // Adjust the duration as needed
    }, currentDelay);
  });
};

animate()