makeToTable = (table, rowName) => {
    var thing = {}

    for (let key in table) {
        thing[key] = table[key][rowName];
    }

    return thing;
}


writeTable = (items, tops, div) => {
    tableTypes = makeToTable(items, "description")

    headingStuffs = $("<tr>")

    for (i in tops){
        header = $("<th>")
            .attr("scope", "col")
            .text(tops[i])

        headingStuffs.append(header)
    }

    tableHeader = $("<thead>")
        .append(headingStuffs)

    body = $("<tbody>")

    Object.entries(tableTypes).forEach(([key, value]) => {
        newRow = $("<tr>")
        
        newRow.append(
            $("<td>")
                .attr("scope", "row")
                .append(
                    $("<code>")
                        .text(key)
                ),
            $("<td>")
                .text(value)
        )

        body.append(newRow)
    })

    table = $("<table>")
        .attr("class", "table animate")
        .append(tableHeader)
        .append(body)

    div.append(table)
}

$("remove").text("Loading, please wait!")

request = {
    "type":"tag-list",
    "key":"ZHt_VYRcoViZ75WFWhT9zA",
    "data":{
    }
}

sendData(JSON.stringify(request)).then(data => {
    processedData = JSON.parse(data).data

    tagDiv = $("#tagDiv")

    tagDiv.append(
        $("<h1>")
            .text("Categories")
            .addClass("animate")
    )

    writeTable(processedData.types, ["Category", "Description"], tagDiv)

    tagDiv.append(
        $("<h1>")
            .text("Tag-list")
            .addClass("animate")
    )
    writeTable(processedData.tags, ["Tag", "Description"], tagDiv)

    $("#tagDiv").children().addClass("m-4")
    $("#remove").hide()
    animate()
}).catch(error => {
    $("#remove").text("An error occurred!<br>" + error)
    console.error('Error:', error);
})
