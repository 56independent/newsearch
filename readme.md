# Advert
Newsearch is a revolutionary new search system, using a tag-based system to allow web navigation.

All you need to do is type in a handful of tags and all websites matching those tags are matched!

# Deployment
Newsearch is not deployment-ready yet. It is a yet-unfinished project. Please wait for me to develop it.

After development is complete, this should be the workflow:

Newsearch is a client-server product. The server has an API to communicate with and can be started using `node server.js`, or using the docker image (not implemented yet).

API keys can be controlled in `keys.json`. Each key is a key in a JSON object, and the permissions come as a string array in the value:

`"iFIuo0B7urBzRUpJSNy7RQ":["tag-list", "read", "write", "waste-memory"],`

The `*` key is used for default permissions, those that do not need a key.

Here is what each permission does:

| `tag-list` | Allows a client to view all tags |
| `read` | Allows a client to read the website directory |
| `write` | Allows a client to directly write to the website directory |
| `waste-memory` | Allows a client high-memory operations like searching|


The client thing itself is web-ready. You just host the client directory and the website will then run itself upon connection.

# API
API messages must arrive as a JSON object. The API is stateless; this makes it simpler to implement and use.

The object contains:

| `type` | The type of message; used by the server to find the operation to do |
| `key` | The API key |
| `data` | Any data needed for the type |

Each type:

| Name | Data needed | Description |
| `tag-list` | none | The server will reply with a directory of tags and their names |
