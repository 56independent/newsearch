const http = require('http');
const cors = require('cors');
const fs = require('fs');
const escapeStringRegexp = require('escape-string-regexp');
// const database = require("databaseWrapper.js")

const corsOptions = {
    origin: '*', // Replace '*' with the appropriate origin URL
};

const authenticate = (key) => {
    keys = JSON.parse(fs.readFileSync("websites.json", "utf-8"))

    permissions = keys.key || keys["*"]
}

function isSubset(subset, superset) {
    return subset.every(item => superset.includes(item));
}

const server = http.createServer((req, res) => {
    let data = '';
    req.on('data', chunk => {
        data += chunk;
    });

    req.on('end', () => {
        //data = escapeStringRegexp(data);

        console.log('Request data:', data);

        var response = {
            "type":"",
            "data":{
            }
        }

        try {
            request = JSON.parse(data)
        } catch {
            console.log("the data, \n\n" + data + "\n\ntasted sour.")

            request = {}
        }

        // TODO: Move logic somewhere better!

        if (request.type == "tag-list" && isSubset(["tag-list"], authenticate(request.key))){
            tagList = JSON.parse(fs.readFileSync("./tagList.json", "utf-8"))

            response.data = tagList
            response.type = "tag-list-reply"
        } else if (request.type == "list" && isSubset(["read"], authenticate(request.key))) {
            websites = JSON.parse(fs.readFileSync("websites.json", "utf-8"))

            response.data = websites.websites
            response.type = "list-response"
        } else if (request.type == "add-website" && isSubset(["write"], authenticate(request.key))) {
            for (website in request.data){
                if (website.length == 4){
                    console.log(website)
                }
            }
        } else if (request.type == "search" && isSubset(["write", "waste-memory"], authenticate(request.key))){
            websites = JSON.parse(fs.readFileSync("websites.json", "utf-8")).websites

            requestedTags = request.data.tags

            results = []

            for (const website of websites) {
                const websiteTags = website[3];
                if (requestedTags.every(requestedTag => websiteTags.includes(requestedTag))) {
                  results.push(website);
                }
            }

            response.data = results
            response.type = "search-results"
        }

        // Enable CORS
        cors(corsOptions)(req, res, () => {
            // Send HTTP response
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(response));
            res.end();
        });
    })
});

const PORT = 5000;
server.listen(PORT, '127.0.0.1', () => {
    console.log(`Server listening on port ${PORT}`);
});
